const schema = require('../data/schema.js');
const { v4: uuidv4 } = require('uuid');
const path = require('path');
const cloudflare = require('../modules/cloudflare.js');
//const AWS = require('aws-sdk');
//AWS.config.region = process.env.AWS_REGION || 'us-east-1';
//const AWS = require('aws-sdk');
//AWS.config.region = process.env.AWS_REGION || 'us-east-1';

/*
/*
const s3 = new AWS.S3({
  endpoint: new AWS.Endpoint(process.env.AWS_ENDPOINT)
});
*/
*/

module.exports = {
  // Note: Create is handled by TUS

  readAll: async ({ knex, token, cb }) => {
    console.log(token);
    let q =
      token.role === 'websuper'
        ? await knex.select('*').from('assets')
        : await knex
  // Note: Create is handled by TUS

  readAll: async ({ knex, token, cb }) => {
    console.log(token);
    let q =
      token.role === 'websuper'
        ? await knex.select('*').from('assets')
        : await knex
            .select('*')
            .from('assets')
            .where({ created_by: token.uuid });
    const asset = q.map((el) => ({
      ...el,
      asset_metadata: JSON.parse(el.asset_metadata)
    }));
    // //.where({ id });
    cb(asset);
  },

  read: async ({ knex, uuid, cb }) => {
    const q = await knex.select('*').from('assets').where({ uuid });
    const assets = q.map((el) => ({
      ...el,
      asset_metadata: JSON.parse(el.asset_metadata)
    }));
    cb(assets[0]);
  read: async ({ knex, uuid, cb }) => {
    const q = await knex.select('*').from('assets').where({ uuid });
    const assets = q.map((el) => ({
      ...el,
      asset_metadata: JSON.parse(el.asset_metadata)
    }));
    cb(assets[0]);
  },

  update: async ({ knex, uuid, data, cb }) => {
    try {
      await knex('assets').where({ uuid }).update(data);
    } catch (e) {
      throw e;
    }
    const q = await knex.select('*').from('assets').where({ uuid });
    cb({ ...q[0], asset_metadata: JSON.parse(q[0].asset_metadata) });
  update: async ({ knex, uuid, data, cb }) => {
    try {
      await knex('assets').where({ uuid }).update(data);
    } catch (e) {
      throw e;
    }
    const q = await knex.select('*').from('assets').where({ uuid });
    cb({ ...q[0], asset_metadata: JSON.parse(q[0].asset_metadata) });
  },

  destroy: async ({ knex, uuid, data, cb }) => {
    let existingAsset = await knex.select('*').from('assets').where({ id });
    if (existingAsset.length === 0)
      existingAsset = await knex
        .select('*')
        .from('assets')
        .where({ asset_path: id });

    const { asset_type, asset_path } = existingAsset[0] || {};
    if (asset_type && !asset_type.includes('custom'))
      _s3DeleteObject(asset_path);
    const q = await knex('assets').where({ id }).del();
    cb(q);
  }

  /*
  replace: async (req, res, cb) => {
    const { asset_path } = req.params;
    const { asset_type, asset_metadata, display_name } = req.body;
    const { uuid } = req.token;
    let existingAsset = await knex
      .select('*')
      .from('assets')
      .where({ asset_path });
    existingAsset = existingAsset[0];
    const new_asset_path = `${uuidv4()}${path.extname(asset_path)}`;
    if (existingAsset) {
      _s3DeleteObject(existingAsset.asset_path);
      const data = {
        asset_path: new_asset_path,
        updated_at: Date.now(),
        display_name,
        asset_type,
        asset_metadata: JSON.stringify({
          ...existingAsset.asset_metadata,
          ...asset_metadata
        })
      };
      if (asset_type.includes('custom')) {
        await knex('assets').where({ id: existingAsset.id }).update(data);
        const q = await knex
          .select('*')
          .from('assets')
          .where({ id: existingAsset });
        cb({ id: existingAsset.id, ...q[0] });
      } else {
        signedRequest = _s3GetSignature(
          new_asset_path,
          asset_type,
          async (signedRequest) => {
            await knex('assets').where({ id: existingAsset.id }).update(data);
            cb({ id: existingAsset.id, ...data, signedRequest });
          }
        );
      }
    } else {
      let data = {
        uuid: uuidv4(),
        display_name: display_name ? display_name : asset_path,
        asset_type,
        asset_path: asset_type.includes('custom') ? asset_path : new_asset_path,
        asset_metadata,
        created_at: Date.now(),
        created_by: uuid,
        asset_metadata
      };
      if (asset_type.includes('custom')) {
        const newId = await req.knex.insert(data).into('assets');
        const q = await req.knex
          .select('*')
          .from('assets')
          .where({ id: newId });
        cb(q[0]);
      } else {
        signedRequest = _s3GetSignature(
          new_asset_path,
          asset_type,
          async (signedRequest) => {
            const newId = await req.knex.insert(data).into('assets');
            const q = await req.knex
              .select('*')
              .from('assets')
              .where({ id: newId });
            cb({ ...q[0], signedRequest });
          }
        );
      }
    }
  }
  */
};

/*
const _s3DeleteObject = (asset_path) => {
  s3.deleteObjects(
    {
      Bucket: process.env.AWS_BUCKET,
      Delete: {
        Objects: [{ Key: `${process.env.AWS_APP_ID}/${asset_path}` }]
      }
    },
    (err) => {
      if (err) {
        console.error(err, err.stack);
      } else {
        cloudflare.purge([
          `https://${process.env.AWS_BUCKET}/${process.env.AWS_APP_ID}/${asset_path}`
        ]);
      }
    }
  );
};

const _s3GetSignature = (name, type, cb) => {
  s3.getSignedUrl(
    'putObject',
    {
      Bucket: process.env.AWS_BUCKET,
      Key:
        process.env.AWS_APP_ID.trim().length > 0
          ? `${process.env.AWS_APP_ID}/${name}`
          : name,
      Expires: 60,
      ContentType: type
    },
    (err, signedRequest) => {
      if (err) {
        console.error(err);
        cb();
      }
      cb(signedRequest);
    }
  );
};
*/
