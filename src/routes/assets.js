const router = require('express').Router();
var asset = require('../models/asset');
const { RESPONSE } = require('../constants.js');
const { requireInParams } = require('../models/validation.js');

router.get('/', (req, res) => {
  asset.readAll({
    knex: req.knex,
    token: req.token,
    cb: (data) => res.status(RESPONSE.OK).send(data)
  });
});

module.exports = router;
