# Package: @theplacelab/auth

React client support for [Auth Service](https://gitlab.com/theplacelab/service/auth).

[[README: Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)]

[![Semantic Release Badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/auth/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/auth/-/pipelines/latest)

# Development

Use [yalc](https://github.com/wclr/yalc) to [solve issues with link](https://divotion.com/blog/yalc-npm-link-alternative-that-does-work). Install yalc, then:

In this package:

```
yalc publish
yalc push
```

Where you want to use it:

```
yalc link @theplacelab/auth
```

# How to Use

- Get a Personal Access Token from Gitlab

- Add an `.npmrc` to your project using the token in place of ~GITLAB_TOKEN~ (or alternately add this to your ~/.npmrc or use config to do that)

  ```
  @theplacelab:registry=https://gitlab.com/api/v4/packages/npm/
  //gitlab.com/api/v4/packages/npm/:_authToken="~GITLAB_TOKEN~"
  ```

  or

  ```
  npm config set @theplacelab:registry https://gitlab.com/api/v4/packages/npm/
  npm config set //gitlab.com/api/v4/packages/npm/:_authToken '~GITLAB_TOKEN~'
  ```

- `yarn add @theplacelab/auth`

# In This Package:

## `<SignIn>`

```js
<SignIn
  loginText="Sign In"
  logoutText="Sign Out"
  showUsername={true}
  authService={window._env_.REACT_APP_AUTH_SERVICE}
  authHelper={window._env_.REACT_APP_AUTH_HELPER}
  onLogin={(payload) => dispatch(userLogin(payload))}
  onLogout={(payload) => dispatch(userLogout(payload))}
  helpLink={
    <Link target="_top" to="/user/help">
      Trouble signing in?
    </Link>
  }
  style={{
    container: {},
    title: {},
    subtitle: {},
    button: {},
    label: {},
    label_bad: {},
    modal: {},
  }}
/>
```

## `<Login>`

```js
<Login
  authService={window._env_.REACT_APP_AUTH_SERVICE}
  authHelper={window._env_.REACT_APP_AUTH_HELPER}
  onAvatarClick={() => window.alert("hi!")}
  onLogin={(payload) => dispatch(userLogin(payload))}
  onLogout={(payload) => dispatch(userLogout(payload))}
/>
```

## `<Logout>`

```js
<Logout confirmation="Are you certain?">Exit</Logout>
```
